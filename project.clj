(defproject scrapper "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-3165"]
                 [ring "1.3.2"]
                 [compojure "1.1.6"]
                 [hiccup "1.0.5"]
                 [com.novemberain/monger "2.0.0"]
                 [enlive "1.1.5"]
                 [cljsjs/d3 "3.5.5-3"]
                 ]

  :plugins [[lein-cljsbuild "1.0.4"]
            [lein-ring "0.9.1"]
            [lein-cljfmt "0.1.10"]]

  :ring {:handler scrapper.handler/app}

  :cljsbuild {:builds [{:source-paths ["src-cljs"]
                        :compiler {:output-to "resources/public/js/main.js"
                                   :optimizations :whitespace
                                   :pretty-print false}}]}

  :main ^:skip-aot scrapper.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
  :dev {:dependencies [[com.cemerick/piggieback "0.2.1"]
                                [org.clojure/tools.nrepl "0.2.10"]]
                 :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}})
